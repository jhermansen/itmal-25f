import json

class Dummy():
    def hello_world(self):
        print("Hello World!")

class Point():
    def __init__(self, x, y):
        self.__x = x
        self.__y = y
    
    @property
    def coordinate(self):
        return {'x': self.__x, 'y': self.__y}

    def __del__(self):
        self.__x = 0 #Not a proper dispose
        self.__y = 0

    def __str__(self):
        return json.dumps({'x': self.__x, 'y': self.__y})

