import matplotlib
import matplotlib.pyplot as plt
import numpy as np
from sklearn.datasets import make_moons
from pandas import DataFrame
from keras import backend as K
K.tensorflow_backend._get_available_gpus()

def MOON_GetDataSet(n_samples):
    return make_moons(noise=0.15, random_state=5, n_samples=n_samples)
        
from sklearn.model_selection import train_test_split

def MOON_Plot(X, y, title="default", xlabel="", ylabel=""):
    df = DataFrame(dict(x=X[:,0], y=X[:,1], label=y))
    colors = {0:'red', 1:'blue'}
    fig, ax = plt.subplots()
    grouped = df.groupby('label')
    for key, group in grouped:
        group.plot(ax=ax, kind='scatter', title=title, x=xlabel, y=ylabel, label=key, color=colors[key])
    plt.show()

from sklearn.datasets import fetch_mldata
from keras.datasets import mnist

def MNIST_PlotDigit(data):
    image = data.reshape(28, 28)
    plt.imshow(image)

def MNIST_GetDataSet():
    return mnist.load_data()


from sklearn.datasets import load_iris

def IRIS_GetDataSet():
    data = load_iris()
    X = data["data"]
    y = data["target"]
    return X, y

import pandas as pd
from pandas.plotting import scatter_matrix 

def IRIS_Plot(X, y):
    df = pd.DataFrame(X, columns=['SepalLength','SepalWidth', 'PetalLength', 'PetalWidth'])
    
    factor = pd.Series(y)
    classes = list(set(factor))
    palette = ['#e41a1c', '#4eae4b', '#377eb8', 
               '#994fa1', '#ff8101', '#fdfc33', 
               '#a8572c', '#f482be', '#999999']
    color_map = dict(zip(classes,palette))
    colors = factor.apply(lambda group: color_map[group])
    scatter_matrix(df, c=colors, alpha=1, diagonal=None)

